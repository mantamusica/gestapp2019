<?php

use yii\db\Migration;

/**
 * Class m190410_162354_tareas
 */
class m190410_162354_tareas extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('tareas', [
            'id'=>$this->primaryKey(),
            'nombre'=>$this->string(50)->notNull(),
            'descripcion'=>$this->string(50)->notNull(),
            'fechaCreacion'=>$this->dateTime()->append('ON UPDATE CURRENT_TIMESTAMP'),
            'fechaInicio'=>$this->dateTime(),
            'fechaFinal'=>$this->dateTime(),
            'fechaActualizacion'=>$this->dateTime()->append('ON UPDATE CURRENT_TIMESTAMP'),
            'categoriaId'=>$this->integer(10),
            'estadoId'=>$this->integer(10),
            'proyectoId'=>$this->integer(10),
            'prioridadId'=>$this->integer(10),
            'activo'=>$this->integer()->notNull()->defaultValue(0),
        ]);

        $this->createIndex(
            'idx-tareas-categorias_id','tareas','categoriaId'
        );

        $this->addForeignKey('fktareas_categorias_id', 'tareas', 'categoriaId', 'categorias',
            'id', 'cascade', 'cascade');

        $this->createIndex(
            'idx-tareas-estados_id','tareas','estadoId'
        );

        $this->addForeignKey('fktareas_estados_id', 'tareas', 'estadoId', 'estados',
            'id', 'cascade', 'cascade');


        $this->createIndex(
            'idx-tareas-proyectos_id','tareas','proyectoId'
        );

        $this->addForeignKey('fktareas_proyectos_id', 'tareas', 'proyectoId', 'proyectos',
            'id', 'cascade', 'cascade');

        $this->createIndex(
            'idx-tareas-prioridades_id','tareas','prioridadId'
        );

        $this->addForeignKey('fktareas_prioridades_id', 'tareas', 'prioridadId', 'prioridades',
            'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tareas');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190410_162354_tareas cannot be reverted.\n";

        return false;
    }
    */
}
