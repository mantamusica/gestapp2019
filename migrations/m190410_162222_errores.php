<?php

use yii\db\Migration;

/**
 * Class m190410_162222_errores
 */
class m190410_162222_errores extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('errores', [
            'id'=>$this->primaryKey(),
            'mensaje'=>$this->string(50),
            'pagina'=>$this->string(50),
            'linea'=>$this->string(50),
            'fecha'=>$this->date(),
            'hora'=>$this->time(),
            'usuarioId'=>$this->integer(10),
            'activo'=>$this->integer()->notNull()->defaultValue(0),
        ]);

        $this->createIndex(
            'idx-errores-usuarios_id','errores','usuarioId'
        );

        $this->addForeignKey('fkerrores_usuarios_id', 'errores', 'usuarioId', 'usuarios',
            'id', 'cascade', 'cascade');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('errores');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190410_162222_errores cannot be reverted.\n";

        return false;
    }
    */
}
