<?php

use yii\db\Migration;

/**
 * Class m190410_162242_roles
 */
class m190410_162242_roles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('roles', [
            'id'=>$this->primaryKey(),
            'nombre'=>$this->string(50)->notNull(),
            'siglas'=>$this->string(2)->notNull(),
            'descripcion'=>$this->string(50)->notNull(),
            'activo'=>$this->integer()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('prioridades');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190410_162242_roles cannot be reverted.\n";

        return false;
    }
    */
}
