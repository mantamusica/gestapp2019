<?php

use yii\db\Migration;

/**
 * Class m190410_162337_proyectos
 */
class m190410_162337_proyectos extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('proyectos', [
            'id'=>$this->primaryKey(),
            'nombre'=>$this->string(50)->notNull(),
            'descripcion'=>$this->string(50)->notNull(),
            'fechaCreacion'=>$this->dateTime()->append('ON UPDATE CURRENT_TIMESTAMP'),
            'fechaInicio'=>$this->dateTime(),
            'fechaFinal'=>$this->dateTime(),
            'fechaActualizacion'=>$this->dateTime()->append('ON UPDATE CURRENT_TIMESTAMP'),
            'usuarioId'=>$this->integer(10),
            'activo'=>$this->integer()->notNull()->defaultValue(0),
        ]);

        $this->createIndex(
            'idx-proyectos-usuarios','proyectos','usuarioId'
        );

        $this->addForeignKey('fkusuarios_proyectos_id', 'proyectos', 'usuarioId', 'usuarios',
            'id', 'cascade', 'cascade');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('proyectos');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190410_162337_proyectos cannot be reverted.\n";

        return false;
    }
    */
}
