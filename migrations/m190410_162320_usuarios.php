<?php

use yii\db\Migration;

/**
 * Class m190410_162320_usuarios
 */
class m190410_162320_usuarios extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('usuarios', [
            'id'=>$this->primaryKey(),
            'nombre'=>$this->string(50),
            'apellidos'=>$this->string(50),
            'email'=>$this->string(50),
            'password'=>$this->string(512),
            'telefono'=>$this->string(9),
            'ultimoAcceso'=>$this->dateTime()->notNull(),
            'fechaRegistro'=>$this->dateTime()->notNull(),
            'contadorAccesos'=>$this->integer(100),
            'contadorAccesosFallidos'=>$this->integer(100),
            'rolId'=>$this->integer(10),
            'bloqueado'=>$this->integer()->notNull()->defaultValue(0),
            'activo'=>$this->integer()->notNull()->defaultValue(0),
        ]);

        $this->createIndex(
            'idx-usuarios-roles_id','usuarios','rolId'
        );

        $this->addForeignKey('fkusuarios_roles_id', 'usuarios', 'rolId', 'roles',
            'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('usuarios');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190410_162320_usuarios cannot be reverted.\n";

        return false;
    }
    */
}
