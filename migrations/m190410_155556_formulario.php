<?php

use yii\db\Migration;

/**
 * Class m190410_155556_formulario
 */
class m190410_155556_formulario extends Migration
{
    public $a;
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('formulario', [
            'idFormulario'=>$this->primaryKey(),
            'nombre'=>$this->string(50)->notNull(),
            'apellidos'=>$this->string(50)->notNull(),
            'email'=>$this->string(50)->notNull(),
            'tema'=>$this->string(30)->notNull(),
            'mensaje'=>$this->string(250)->notNull(),
            'fecha'=>$this->dateTime()->append('ON UPDATE CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('formulario');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190410_155556_formulario cannot be reverted.\n";

        return false;
    }
    */
}
