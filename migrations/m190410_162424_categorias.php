<?php

use yii\db\Migration;

/**
 * Class m190410_162424_categorias
 */
class m190410_162424_categorias extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('categorias', [
            'id'=>$this->primaryKey(),
            'nombre'=>$this->string(50)->notNull(),
            'descripcion'=>$this->string(50)->notNull(),
            'activo'=>$this->integer()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('categorias');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190410_162424_categorias cannot be reverted.\n";

        return false;
    }
    */
}
