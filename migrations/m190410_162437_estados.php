<?php

use yii\db\Migration;

/**
 * Class m190410_162437_estados
 */
class m190410_162437_estados extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('estados', [
            'id'=>$this->primaryKey(),
            'nombre'=>$this->string(50)->notNull(),
            'descripcion'=>$this->string(50)->notNull(),
            'activo'=>$this->integer()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('estados');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190410_162437_estados cannot be reverted.\n";

        return false;
    }
    */
}
