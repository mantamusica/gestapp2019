<?php

use yii\db\Migration;

/**
 * Class m190410_162253_logs
 */
class m190410_162253_logs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('logs', [
            'id'=>$this->primaryKey(),
            'fecha'=>$this->date(),
            'hora'=>$this->time(),
            'usuarioId'=>$this->integer(10),
            'activo'=>$this->integer()->notNull()->defaultValue(0),
        ]);

        $this->createIndex(
            'idx-logs-usuarios_id','logs','usuarioId'
        );

        $this->addForeignKey('fklogs_usuarios_id', 'logs', 'usuarioId', 'usuarios',
            'id', 'cascade', 'cascade');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('logs');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190410_162253_logs cannot be reverted.\n";

        return false;
    }
    */
}
