<?php

use yii\db\Migration;

/**
 * Class m190410_162307_sesiones
 */
class m190410_162307_sesiones extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('sesiones', [
            'id'=>$this->primaryKey(),
            'fechaCreacion'=>$this->dateTime()->append('ON UPDATE CURRENT_TIMESTAMP'),
            'usuarioId'=>$this->integer(10),
            'activo'=>$this->integer()->notNull()->defaultValue(0),
        ]);

        $this->createIndex(
            'idx-sesiones-usuarios_id','sesiones','usuarioId'
        );

        $this->addForeignKey('fksesiones_usuarios_id', 'sesiones', 'usuarioId', 'usuarios',
            'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sesiones');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190410_162307_sesiones cannot be reverted.\n";

        return false;
    }
    */
}
