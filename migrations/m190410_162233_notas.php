<?php

use yii\db\Migration;

/**
 * Class m190410_162233_notas
 */
class m190410_162233_notas extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('notas', [
            'id'=>$this->primaryKey(),
            'titulo'=>$this->string()->notNull(),
            'descripcion'=>$this->string()->notNull(),
            'usuarioId'=>$this->integer(10),
            'activo'=>$this->integer()->notNull()->defaultValue(0),
        ]);

        $this->createIndex(
            'idx-notas-usuarios_id','notas','usuarioId'
        );

        $this->addForeignKey('fknotas_usuarios_id', 'notas', 'usuarioId', 'usuarios',
            'id', 'cascade', 'cascade');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('notas');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190410_162233_notas cannot be reverted.\n";

        return false;
    }
    */
}
