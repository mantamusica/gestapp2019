<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "formulario".
 *
 * @property int $id_formulario
 * @property string $nombre
 * @property string $apellidos
 * @property string $email
 * @property string $tema
 * @property string $mensaje
 */
class Formulario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'formulario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellidos', 'email', 'tema', 'mensaje'], 'required'],
            [['nombre'], 'string', 'max' => 50],
            [['apellidos', 'email', 'tema'], 'string', 'max' => 100],
            [['mensaje'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_formulario' => 'Id Formulario',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'email' => 'Email',
            'tema' => 'Tema',
            'mensaje' => 'Mensaje',
        ];
    }
}
