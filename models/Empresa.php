<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "empresa".
 *
 * @property int $id_empresa
 * @property string $nombre
 * @property string $direccion
 * @property string $cif
 * @property string $telefono
 * @property string $email
 * @property int $activo
 *
 * @property Departamento[] $departamentos
 */
class Empresa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empresa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre','direccion','cif','telefono','email'], 'required'],
            [['activo'], 'integer'],
            [['nombre'], 'string', 'max' => 128],
            [['direccion'], 'string', 'max' => 256],
            [['nombre'], 'unique'],
            [['email','cif'], 'unique'],
            [['email'], 'email']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_empresa' => 'Id Empresa',
            'nombre' => 'Nombre',
            'direccion' => 'Direccion',
            'cif' => 'CIF',
            'telefono' => 'Telefono',
            'email' => 'Email',
            'activo' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartamentos()
    {
        return $this->hasMany(Departamento::className(), ['empresa_id' => 'id_empresa']);
    }

    /**
     * {@inheritdoc}
     * @return CategoriaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EmpresaQuery(get_called_class());
    }

    public static function getEmpresas()
    {
        $empresas = Self::find()->all();

        return ArrayHelper::map($empresas, 'id_empresa', 'nombre');

    }

    public function getLastId(){
        return $id=self::find()
            ->select('id_empresa')
            ->orderBy(['id_empresa' =>SORT_DESC])
            ->limit(1)
            ->one();
    }
}
