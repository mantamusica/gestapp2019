<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "prioridad".
 *
 * @property int $id_prioridad
 * @property string $nombre
 * @property string $descripcion
 * @property int $activo
 *
 * @property Tarea[] $tareas
 */
class Prioridad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'prioridad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activo'], 'integer'],
            [['nombre'], 'string', 'max' => 128],
            [['descripcion'], 'string', 'max' => 256],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_prioridad' => 'Id Prioridad',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'activo' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTareas()
    {
        return $this->hasMany(Tarea::className(), ['prioridad_id' => 'id_prioridad']);
    }

    /**
     * {@inheritdoc}
     * @return PrioridadQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PrioridadQuery(get_called_class());
    }

    public static function getPrioridades()
    {
        $prioridades = Self::find()->all();

        return ArrayHelper::map($prioridades, 'id_prioridad', 'nombre');

    }
}
