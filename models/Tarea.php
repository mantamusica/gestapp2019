<?php

namespace app\models;

use Yii;
use DateTimeZone;
use DateTime;

/**
 * This is the model class for table "tarea".
 *
 * @property int $id_tarea
 * @property string $nombre
 * @property string $descripcion
 * @property string $fecha_creacion
 * @property string $fecha_inicio
 * @property string $fecha_final
 * @property string $fecha_update
 * @property int $categoria_id
 * @property int $estado_id
 * @property int $proyecto_id
 * @property int $prioridad_id
 * @property int $activo
 *
 * @property Proyecto $proyecto
 * @property Categoria $categoria
 * @property Estado $estado
 * @property Prioridad $prioridad
 */
class Tarea extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tarea';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'fecha_creacion', 'fecha_inicio', 'fecha_final', 'fecha_update', 'categoria_id', 'estado_id', 'proyecto_id', 'prioridad_id'], 'required'],
            [['fecha_creacion', 'fecha_inicio', 'fecha_final', 'fecha_update'], 'safe'],
            [['categoria_id', 'estado_id', 'proyecto_id', 'prioridad_id', 'activo'], 'integer'],
            [['nombre'], 'string', 'max' => 128],
            [['descripcion'], 'string', 'max' => 256],
            [['proyecto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Proyecto::className(), 'targetAttribute' => ['proyecto_id' => 'id_proyecto']],
            [['categoria_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categoria::className(), 'targetAttribute' => ['categoria_id' => 'id_categoria']],
            [['estado_id'], 'exist', 'skipOnError' => true, 'targetClass' => Estado::className(), 'targetAttribute' => ['estado_id' => 'id_estado']],
            [['prioridad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prioridad::className(), 'targetAttribute' => ['prioridad_id' => 'id_prioridad']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_tarea' => 'Id Tarea',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_final' => 'Fecha Final',
            'fecha_update' => 'Fecha Update',
            'categoria_id' => 'Categoria ID',
            'estado_id' => 'Estado ID',
            'proyecto_id' => 'Proyecto ID',
            'prioridad_id' => 'Prioridad ID',
            'categoria.nombre' => 'Categoria',
            'estado.nombre' => 'Estado',
            'proyecto.nombre' => 'Proyecto',
            'prioridad.nombre' => 'Prioridad',
            'activo' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyecto()
    {
        return $this->hasOne(Proyecto::className(), ['id_proyecto' => 'proyecto_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria()
    {
        return $this->hasOne(Categoria::className(), ['id_categoria' => 'categoria_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstado()
    {
        return $this->hasOne(Estado::className(), ['id_estado' => 'estado_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrioridad()
    {
        return $this->hasOne(Prioridad::className(), ['id_prioridad' => 'prioridad_id']);
    }

    /**
     * {@inheritdoc}
     * @return TareaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TareaQuery(get_called_class());
    }
    public function getDatetimeNow() {
        $tz_object = new DateTimeZone('Europe/Madrid');
        //date_default_timezone_set('Brazil/East');

        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y\-m\-d\ h:i:s');
    }

}
