<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use DateTime;
use DateTimeZone;

/**
 * This is the model class for table "usuario".
 *
 * @property int $id_usuario
 * @property string $nombre
 * @property string $apellidos
 * @property string $email
 * @property string $password
 * @property int $telefono
 * @property string $ultimo_acceso
 * @property string $fecha_registro
 * @property int $contador_accesos
 * @property int $contador_accesos_fallidos
 * @property int $rol_id
 * @property int $bloqueado
 * @property int $activo
 *
 * @property Error[] $errors0
 * @property Log[] $logs
 * @property Nota[] $notas
 * @property Proyecto[] $proyectos
 * @property Sesion[] $sesions
 * @property Rol $rol
 */
class Usuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellidos', 'email', 'password', 'ultimo_acceso', 'fecha_registro', 'contador_accesos', 'contador_accesos_fallidos'], 'required'],
            [['telefono', 'contador_accesos', 'contador_accesos_fallidos', 'rol_id','departamento_id', 'bloqueado', 'activo'], 'integer'],
            [['ultimo_acceso', 'fecha_registro'], 'safe'],
            [['nombre', 'email'], 'string', 'max' => 128],
            [['apellidos', 'password'], 'string', 'max' => 256],
            [['email'], 'unique'],
            [['rol_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rol::className(), 'targetAttribute' => ['rol_id' => 'id_rol']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_usuario' => 'Id',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'email' => 'Email',
            'password' => 'Password',
            'telefono' => 'Telefono',
            'ultimo_acceso' => 'Ultimo',
            'fecha_registro' => 'Registro',
            'contador_accesos' => 'Accesos',
            'contador_accesos_fallidos' => 'Fallidos',
            'departamento_id' => 'Departamento',
            'rol_id' => 'Rol ID',
            'bloqueado' => 'Bloqueado',
            'activo' => 'Activo',
            'rol.nombre' => 'Rol',
            'departamento.nombre' => 'Departamento',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getErrors0()
    {
        return $this->hasMany(Error::className(), ['usuario_id' => 'id_usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Log::className(), ['usuario_id' => 'id_usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotas()
    {
        return $this->hasMany(Nota::className(), ['usuario_id' => 'id_usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProyectos()
    {
        return $this->hasMany(Proyecto::className(), ['usuario_id' => 'id_usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSesions()
    {
        return $this->hasMany(Sesion::className(), ['usuario_id' => 'id_usuario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRol()
    {
        return $this->hasOne(Rol::className(), ['id_rol' => 'rol_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartamento()
    {
        return $this->hasOne(Departamento::className(), ['id_departamento' => 'departamento_id']);
    }

    /**
     * {@inheritdoc}
     * @return UsuarioQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsuarioQuery(get_called_class());
    }

    public static function getUsuarios()
    {
        $usuarios = Self::find()->all();

        return ArrayHelper::map($usuarios, 'id_usuario', 'nombre');

    }
    public function getDatetimeNow() {
        $tz_object = new DateTimeZone('Europe/Madrid');
        //date_default_timezone_set('Brazil/East');

        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y\-m\-d\ h:i:s');
    }



}
