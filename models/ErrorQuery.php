<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Error]].
 *
 * @see Error
 */
class ErrorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Error[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Error|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
