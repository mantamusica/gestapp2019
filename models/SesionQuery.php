<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Sesion]].
 *
 * @see Sesion
 */
class SesionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Sesion[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Sesion|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
