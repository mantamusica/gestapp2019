<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tarea;

/**
 * TareaSearch represents the model behind the search form of `app\models\Tarea`.
 */
class TareaSearch extends Tarea
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tarea', 'categoria_id', 'estado_id', 'proyecto_id', 'prioridad_id', 'activo'], 'integer'],
            [['nombre', 'descripcion', 'fecha_creacion', 'fecha_inicio', 'fecha_final', 'fecha_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tarea::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_tarea' => $this->id_tarea,
            'fecha_creacion' => $this->fecha_creacion,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_final' => $this->fecha_final,
            'fecha_update' => $this->fecha_update,
            'categoria_id' => $this->categoria_id,
            'estado_id' => $this->estado_id,
            'proyecto_id' => $this->proyecto_id,
            'prioridad_id' => $this->prioridad_id,
            'activo' => $this->activo,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
