<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "error".
 *
 * @property int $id_error
 * @property string $mensaje
 * @property string $pagina
 * @property string $linea
 * @property string $fecha
 * @property int $usuario_id
 *
 * @property Usuario $usuario
 */
class Error extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'error';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['usuario_id'], 'integer'],
            [['mensaje', 'pagina', 'linea'], 'string', 'max' => 256],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id_usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_error' => 'Id Error',
            'mensaje' => 'Mensaje',
            'pagina' => 'Pagina',
            'linea' => 'Linea',
            'fecha' => 'Fecha',
            'usuario_id' => 'Usuario ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id_usuario' => 'usuario_id']);
    }

    /**
     * {@inheritdoc}
     * @return ErrorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ErrorQuery(get_called_class());
    }
}
