<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Prioridad]].
 *
 * @see Prioridad
 */
class PrioridadQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Prioridad[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Prioridad|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
