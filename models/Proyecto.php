<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use DateTimeZone;
use DateTime;

/**
 * This is the model class for table "proyecto".
 *
 * @property int $id_proyecto
 * @property string $nombre
 * @property string $descripcion
 * @property string $fecha_creacion
 * @property string $fecha_ini
 * @property string $fecha_fin
 * @property string $fecha_update
 * @property int $usuario_id
 * @property int $activo
 *
 * @property Usuario $usuario
 * @property Tarea[] $tareas
 */
class Proyecto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proyecto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'fecha_creacion', 'fecha_ini', 'fecha_fin', 'fecha_update', 'usuario_id'], 'required'],
            [['fecha_creacion', 'fecha_ini', 'fecha_fin', 'fecha_update'], 'safe'],
            [['usuario_id', 'activo'], 'integer'],
            [['nombre'], 'string', 'max' => 128],
            [['descripcion'], 'string', 'max' => 256],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id_usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_proyecto' => 'Id Proyecto',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'fecha_creacion' => 'Fecha Creacion',
            'fecha_ini' => 'Fecha Ini',
            'fecha_fin' => 'Fecha Fin',
            'fecha_update' => 'Fecha Update',
            'usuario_id' => 'Usuario ID',
            'activo' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id_usuario' => 'usuario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTareas()
    {
        return $this->hasMany(Tarea::className(), ['proyecto_id' => 'id_proyecto']);
    }

    /**
     * {@inheritdoc}
     * @return ProyectoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProyectoQuery(get_called_class());
    }

    public static function getProyectos()
    {
        $proyectos = Self::find()->all();

        return ArrayHelper::map($proyectos, 'id_proyecto', 'nombre');

    }
    public function getDatetimeNow() {
        $tz_object = new DateTimeZone('Europe/Madrid');
        //date_default_timezone_set('Brazil/East');

        $datetime = new DateTime();
        $datetime->setTimezone($tz_object);
        return $datetime->format('Y\-m\-d\ h:i:s');
    }
}
