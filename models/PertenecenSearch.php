<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pertenecen;

/**
 * PertenecenSearch represents the model behind the search form of `app\models\Pertenecen`.
 */
class PertenecenSearch extends Pertenecen
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pertenecen'], 'integer'],
            [['empresa_id', 'departamento_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pertenecen::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pertenecen' => $this->id_pertenecen,
        ]);

        $query->andFilterWhere(['like', 'empresa_id', $this->empresa_id])
            ->andFilterWhere(['like', 'departamento_id', $this->departamento_id]);

        return $dataProvider;
    }
}
