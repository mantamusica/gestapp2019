<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sesion".
 *
 * @property int $id_sesion
 * @property string $fecha
 * @property int $usuario_id
 *
 * @property Usuario $usuario
 */
class Sesion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sesion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'usuario_id'], 'required'],
            [['fecha'], 'safe'],
            [['usuario_id'], 'integer'],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id_usuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_sesion' => 'Id Sesion',
            'fecha' => 'Fecha',
            'usuario_id' => 'Usuario ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id_usuario' => 'usuario_id']);
    }

    /**
     * {@inheritdoc}
     * @return SesionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SesionQuery(get_called_class());
    }
}
