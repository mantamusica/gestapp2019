<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "rol".
 *
 * @property int $id_rol
 * @property string $nombre
 * @property string $siglas
 * @property string $descripcion
 * @property int $activo
 *
 * @property Usuario[] $usuarios
 */
class Rol extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rol';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'siglas'], 'required'],
            [['activo'], 'integer'],
            [['nombre', 'siglas'], 'string', 'max' => 128],
            [['descripcion'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_rol' => 'Id Rol',
            'nombre' => 'Nombre',
            'siglas' => 'Siglas',
            'descripcion' => 'Descripcion',
            'activo' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['rol_id' => 'id_rol']);
    }

    /**
     * {@inheritdoc}
     * @return RolQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RolQuery(get_called_class());
    }

    public static function getRoles()
    {
        $roles = Self::find()->all();

        return ArrayHelper::map($roles, 'id_rol', 'nombre');

    }
}
