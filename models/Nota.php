<?php

namespace app\models;

use app\controllers\NotaController;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "nota".
 *
 * @property int $id_nota
 * @property string $titulo
 * @property string $descripcion
 *  * @property string $archivo_id
 * @property int $usuario_id
 * @property int $activo
 *
 * @property Usuario $usuario
 */
class Nota extends \yii\db\ActiveRecord
{
    public $verArchivo;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nota';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo', 'usuario_id'], 'required'],
            [['usuario_id', 'activo'], 'integer'],
            [['titulo'], 'string', 'max' => 128],
            [['descripcion'], 'string', 'max' => 256],
            [['archivo_id'], 'file',
                'skipOnEmpty' => true,
                'extensions' => 'doc, pdf, docx, txt',
                'wrongExtension' => 'El archivo no contiene una extensión permitida',//error
                //'uploadRequired' => 'No has seleccionado ningún archivo',//error
                'maxSize' => 1024*1024*5, // 5 MB
                'tooBig' => 'El tamaño seleccionado máximo permitido son 5 Mb',
                'minSize' => 10, //10 Bytes
                'tooSmall' => 'El tamaño mínimo permitido son 10 Bytes',//error
                'maxFiles' => 1,
                'tooMany' => 'Solo se puede introducir un archivo.'//error
            ],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id_usuario']],
            [['archivo_id'], 'default', 'value' => NULL]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_nota' => 'Id Nota',
            'titulo' => 'Titulo',
            'descripcion' => 'Descripcion',
            'archivo_id' => 'Archivo',
            'verArchivo'=> 'Archivo',
            'archivo.name' => 'Archivo',
            'usuario.nombre' => 'Nombre Usuario',
            'usuario_id' => 'Usuario ID',
            'activo' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id_usuario' => 'usuario_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArchivo()
    {
        return $this->hasOne(Archivo::className(), ['id_archivo' => 'archivo_id']);
    }

    /**
     * {@inheritdoc}
     * @return NotaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NotaQuery(get_called_class());
    }

    public function upload($model)
    {
        if ($this->validate()) {
            $this->archivo_id->saveAs('doc/' . $model->archivo_id->name);
            return true;
        } else {
            return false;
        }
    }

    public function saveFile($model)
    {
        $archive = new Archivo();

        $data = ArrayHelper::toArray($model->archivo_id);
        $archive->name = $data['name'];
        $archive->tempName = $data['tempName'];
        $archive->type = $data['type'];
        $archive->size = $data['size'];
        $archive->error = $data['error'];

        if($archive->validate())
        {
            $archive->save();
            return true;
        }
        return false;

    }

    public function updateFile($model)
    {
        $modelArchive = Archivo::findOne($model->getOldAttribute("archivo_id"));

        $data = ArrayHelper::toArray($model->archivo_id);

        $modelArchive->name = $data['name'];
        $modelArchive->tempName = $data['tempName'];
        $modelArchive->type = $data['type'];
        $modelArchive->size = $data['size'];
        $modelArchive->error = $data['error'];

        if($modelArchive->validate())
        {
            $modelArchive->save();
            return true;
        }
        return false;

    }
    public function newFile($model)
    {
        $modelArchive = new Archivo();

        $data = ArrayHelper::toArray($model->archivo_id);

        $modelArchive->name = $data['name'];
        $modelArchive->tempName = $data['tempName'];
        $modelArchive->type = $data['type'];
        $modelArchive->size = $data['size'];
        $modelArchive->error = $data['error'];

        if($modelArchive->validate())
        {
            $modelArchive->save();
            return true;
        }
        return false;

    }
}
