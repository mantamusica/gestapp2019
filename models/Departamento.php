<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "departamento".
 *
 * @property int $id_departamento
 * @property string $nombre
 * @property string $descripcion
 * @property int $empresa_id
 * @property int $activo
 *
 * @property Empresa $empresa
 */
class Departamento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'departamento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'direccion'], 'required'],
            [['activo'], 'integer'],
            [['nombre'], 'string', 'max' => 25],
            [['descripcion'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_departamento' => 'Id',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripción',
            'activo' => 'Activo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Error::className(), ['departamento_id' => 'id_departamento']);
    }

    /**
     * {@inheritdoc}
     * @return DepartamentoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DepartamentoQuery(get_called_class());
    }

    public static function getDepartamentos()
    {
        $departamentos = Self::find()->all();

        return ArrayHelper::map($departamentos, 'id_departamento', 'nombre');

    }


}
