<?php

namespace app\models;

use Yii;
use app\models\Departamento;

/**
 * This is the model class for table "pertenecen".
 *
 * @property int $id
 * @property int $empresa_id
 * @property int $departamento_id
 * @property int $activo
 *
 * @property Departamento $departamento0
 * @property Empresa $empresa0
 */
class Pertenecen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pertenecen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['empresa_id', 'departamento_id'], 'integer'],
            [['empresa_id', 'departamento_id'], 'unique', 'message' => 'Esta empresa ya tiene este departamento asignado.'],
            [['departamento_id'], 'exist', 'skipOnError' => true, 'targetClass' => Departamento::className(), 'targetAttribute' => ['departamento_id' => 'id_departamento']],
            [['empresa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_id' => 'id_empresa']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'empresa_id' => 'Nº Empresa',
            'empresa0.nombre' => 'Empresa',
            'departamento_id' => 'Nº Departamento',
            'departamento0.nombre' => 'Departamento',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartamento0()
    {
        return $this->hasOne(Departamento::className(), ['id_departamento' => 'departamento_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa0()
    {
        return $this->hasOne(Empresa::className(), ['id_empresa' => 'empresa_id']);
    }

    /**
     * {@inheritdoc}
     * @return PertenecenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PertenecenQuery(get_called_class());
    }



    public function getDepartamentos($id)
    {
        $departamentos = Self::find()
            ->where(['empresa_id' => $id])
            ->joinWith('departamentos0')
            ->asArray()
            ->all();

        foreach ($departamentos as $data){
            $id = $data['departamentos0']['nombre'];
            $lista[]=$id;
        }
        return $lista;
    }

}