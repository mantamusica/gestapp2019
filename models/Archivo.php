<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "archivo".
 *
 * @property int $id_archivo
 * @property string $name
 * @property string $tempName
 * @property string $type
 * @property int $size
 * @property int $error
 *
 * @property Nota[] $notas
 */
class Archivo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'archivo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['size', 'error'], 'integer'],
            [['name', 'tempName', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_archivo' => 'Id Archivo',
            'name' => 'Name',
            'tempName' => 'Temp Name',
            'type' => 'Type',
            'size' => 'Size',
            'error' => 'Error',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotas()
    {
        return $this->hasMany(Nota::className(), ['archivo_id' => 'id_archivo']);
    }



    public  function getLastId(){
        $id = self::find()
            ->select('id_archivo')
            ->orderBy(['id_archivo' => SORT_DESC])
            ->limit(1)
            ->asArray()
            ->one();

        $id = (int) $id['id_archivo'];

        return $id+1;

    }

    public static function getArchivos($model)
    {
        $archivos = Self::find()->where(['id_archivo' => $model->archivo_id])->all();

        return ArrayHelper::map($archivos, 'id_archivo', 'name');

    }
}
