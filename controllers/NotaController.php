<?php

namespace app\controllers;

use app\models\Archivo;
use Yii;
use app\models\Nota;
use app\models\NotaSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * NotaController implements the CRUD actions for Nota model.
 */
class NotaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Nota models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NotaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 10];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Nota model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model

        ]);
    }

    /**
     * Creates a new Nota model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Nota();

        if ($model->load(Yii::$app->request->post())) {
            $model->archivo_id = UploadedFile::getInstance($model, 'archivo_id');
            if (is_null($model->archivo_id)) {
                $model->save();
                return $this->redirect(['view', 'id' => $model->id_nota]);
            } else {
                if ($model->upload($model)) {
                    Nota::saveFile($model);
                    $model->archivo_id = Archivo::getLastId()-1;
                    $model->validate();
                    $model->save();
                    return $this->redirect(['view', 'id' => $model->id_nota]);
                }
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Nota model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $model->archivo_id = UploadedFile::getInstance($model, 'archivo_id');

            if (is_null($model->archivo_id)){
                $model->archivo_id=$model->getOldAttribute("archivo_id");
                $model->save();
                return $this->redirect(['view', 'id' => $model->id_nota]);
            }else{
                if ($model->upload($model)) {
                    if (is_null($model->getOldAttribute("archivo_id")))
                        {
                            Nota::newFile($model);
                            $model->archivo_id=Archivo::getLastId()-1;
                        }else{
                            Nota::updateFile($model);
                            $model->archivo_id=$model->getOldAttribute("archivo_id");
                        }
                    $model->save();
                    return $this->redirect(['view', 'id' => $model->id_nota]);
                }
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Nota model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Nota model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Nota the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Nota::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
