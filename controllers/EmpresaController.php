<?php

namespace app\controllers;

use Yii;
use app\models\Empresa;
use app\models\EmpresaSearch;
use app\models\Departamento;
use app\models\Pertenecen;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * EmpresaController implements the CRUD actions for Empresa model.
 */
class EmpresaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Empresa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmpresaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' => 10];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Empresa model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $departamentos=Departamento::find()
            ->select('id_departamento, nombre')
            ->where(['empresa_id' => $id] )
            ->joinWith('departamentos')
            ->all();
        $listaDepartamentos=ArrayHelper::map($departamentos,'id_departamento','nombre');

        return $this->render('view', [
            'model' => $this->findModel($id),
            'listaDepartamentos' => $listaDepartamentos
        ]);
    }

    /**
     * Creates a new Empresa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Empresa();

        $departamentos=Departamento::find()->select('id_departamento, nombre')->all();
        $listaDepartamentos=ArrayHelper::map($departamentos,'id_departamento','nombre');
        $departamento = new Departamento();

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            $empresa = $model->id_empresa;
            if(Yii::$app->request->post()['Departamento']['id_departamento'] === ''){
                $pertenecen = new Pertenecen();
                $pertenecen->empresa_id = $empresa;
                $pertenecen->save();
            }else{
                foreach (Yii::$app->request->post()['Departamento'] as $data){
                    $contador = 0;
                    foreach($data as $valor){
                        $contador++;
                        $pertenecen = new Pertenecen();
                        $pertenecen->empresa_id = $empresa;
                        $pertenecen->departamento_id = $valor;
                        var_dump($pertenecen);

                        $pertenecen->validate();
                        $pertenecen->save();
                    }
                    exit;
                }
            }
            return $this->redirect(['view', 'id' => $empresa]);
        }

        return $this->render('create', [
            'model' => $model,
            'departamento' => $departamento,
            'listaDepartamentos' => $listaDepartamentos

        ]);
    }

    /**
     * Updates an existing Empresa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_empresa]);
        }

        return $this->render('create', [
            'model' => $model,
            'departamento' => $departamento,
            'listaDepartamentos' => $listaDepartamentos

        ]);
    }

    /**
     * Deletes an existing Empresa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Empresa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Empresa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Empresa::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
