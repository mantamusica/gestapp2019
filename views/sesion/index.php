<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use dosamigos\datetimepicker\DateTimePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SesionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sesiones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sesion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel'  => 'Última'
        ],
        'layout' => "{pager}\n{items}\n{pager}",
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
                'id_sesion',
            [
                'attribute' => 'fecha',
                'value' => 'fecha',
                'format' => 'raw',
                'filter' => DateTimePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'fecha',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd H:i:s'
                    ]
                ])
            ],
                //'fecha',
                'usuario.nombre',
                'usuario_id',
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
