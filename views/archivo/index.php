<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ArchivoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Archivos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="archivo-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{pager}\n{items}\n{pager}",
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel' => 'Última',
            'prevPageLabel' => 'Anterior',
            'nextPageLabel' => 'Siguiente',
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_archivo',
            'name',
            'tempName',
            'type',
            'size',
            'error',

            ['class' => 'yii\grid\ActionColumn',
                'header' => 'Archivo',
                'template' => '{link}',
                'buttons' => [
                    'link' => function ($url,$model) {
                        $url = 'download?id='.$model->id_archivo;
                        return Html::a('Descargar',$url, ['class' => 'btn btn-danger']);
                    },
                ],
            ],
        ],
    ]); ?>


</div>
