<?php
use yii\helpers\Url;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
$url = Url::to('@web/img/logo2.png')
?>
<div class="back-index">

    <div class="jumbotron">
        <h1>DashBoard GestApp</h1>
        <h1>2019 Yii2 </h1>

        <div class="row">
            <div class="col-lg-4">
            </div>
            <div class="col-lg-4">
                <p><?= Html::img($url, ['width'=>'600px','class' =>'img-responsive']); ?></p>
            </div>
            <div class="col-lg-4">
            </div>
        </div>
    </div>

    <div class="body-content">


    </div>
</div>
