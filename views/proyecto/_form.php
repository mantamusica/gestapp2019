<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Usuario;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Proyecto */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="proyecto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?php if($dato == 'update') { ?>
        <?=$form->field($model, 'fecha_creacion')->textInput(['maxlength' => true, 'disabled' => true]);
    }?>

    <?= $form->field($model, 'fecha_ini')->widget(DatePicker::classname(),[
        'options' => ['placeholder' => 'Introduce la fecha inicial ...'],
        'language' => 'es',
        'inline' => false,
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]]) ?>

    <?= $form->field($model, 'fecha_fin')->widget(DatePicker::classname(),[
        'options' => ['placeholder' => 'Introduce la fecha final ...'],
        'language' => 'es',
        'inline' => false,
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]]) ?>

    <?php if($dato == 'update') { ?>
        <?=$form->field($model, 'fecha_update')->textInput(['maxlength' => true, 'disabled' => true]);
    }?>

    <?= $form->field($model, 'usuario_id')->dropDownList(
        Usuario::getUsuarios(),
        ['prompt'=>'Select...']
    ); ?>

    <?= $form->field($model, 'activo')->dropDownList(
        [0 => 'No Activo', 1 => 'Activo']
    ); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
