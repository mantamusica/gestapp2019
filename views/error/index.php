<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use dosamigos\datetimepicker\DateTimePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ErrorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Errores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="error-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel'  => 'Última'
        ],
        'layout' => "{pager}\n{items}\n{pager}",
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_error',
            'mensaje',
            'pagina',
            'linea',
            [
                'attribute' => 'fecha',
                'value' => 'fecha',
                'format' => 'raw',
                'filter' => DateTimePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'fecha',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd H:i:s'
                    ]
                ])
            ],
            //'fecha',
            'usuario.nombre',
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
