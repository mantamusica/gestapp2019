<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Usuario;

/* @var $this yii\web\View */
/* @var $model app\models\Nota */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nota-form">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => true,
        //'method' => 'get',
        'options' => [
            'enctype' => 'multipart/form-data',
            'validateOnSubmit' => true,
        ]]) ?>

    <?= $form->field($model, 'titulo')->textInput(['maxlength' => true],['placeholder' => 'Introduce una descripción']) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true],['placeholder' => 'Introduce una descripción']) ?>

    <?= $form->field($model, 'archivo_id')->fileInput(['class' => 'btn btn-default btn-file']) ?>

    <?= $form->field($model, 'usuario_id')->dropDownList(
        Usuario::getUsuarios(),
        ['prompt'=>'Select...']
    ); ?>

    <?= $form->field($model, 'activo')->dropDownList(
        [0 => 'No Activo', 1 => 'Activo']
    ); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
