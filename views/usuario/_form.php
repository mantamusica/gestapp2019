<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Rol;
use app\models\Departamento;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php if (is_null($data)) { ?>
        <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
    <?php }?>

    <?= $form->field($model, 'telefono')->textInput() ?>

    <?= $form->field($model, 'rol_id')->dropDownList(
                Rol::getRoles(),
                ['prompt'=>'Selecciona uno ...'])
    ?>

    <?= $form->field($model, 'departamento_id')->dropDownList(
        Departamento::getDepartamentos(),
        ['prompt'=>'Selecciona uno ...'])
    ?>

    <?= $form->field($model, 'bloqueado')->dropDownList(
        [0 => 'Desbloqueado', 1 => 'Bloqueado']
    ); ?>

    <?= $form->field($model, 'activo')->dropDownList(
        [0 => 'No Activo', 1 => 'Activo']
    ); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
