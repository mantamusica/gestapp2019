<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p><?= Html::a('Nuevo Usuario', ['create'], ['class' => 'btn btn-success'])  ?></p>
    <div class="row"></div>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel'  => 'Última'
        ],
        'layout' => "{pager}\n{items}\n{pager}",
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_usuario',
            'nombre',
            'apellidos',
            //'email:email',
            //'password',
            'telefono',
            [
                'attribute' => 'fecha_registro',
                'value' => 'fecha_registro',
                'format' => 'raw',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'fecha_registro',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])
            ],
            //'ultimo_acceso',
            //'fecha_registro',
            //'contador_accesos',
            //'contador_accesos_fallidos',
            'departamento.nombre',
            'rol.nombre',
            'bloqueado',
            'activo',

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'Acciones'
            ]
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
