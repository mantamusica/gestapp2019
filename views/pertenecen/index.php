<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel app\models\PertenecenSearch */

$this->title = 'Gestion de Departamentos de Empresas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pertenecen-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nuevos Departamentos para Empresa', ['empresa/create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{pager}\n{items}\n{pager}",
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel' => 'Última',
            'prevPageLabel' => 'Anterior',
            'nextPageLabel' => 'Siguiente',
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_pertenecen',
            'empresa_id',
            'empresa0.nombre',
            'departamento_id',
            'departamento0.nombre',
            'activo'

        ],
    ]); ?>


    <?php Pjax::end(); ?>
</div>
