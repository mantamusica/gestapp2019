<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use dosamigos\datepicker\DatePicker;
use kartik\time\TimePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel'  => 'Última'
        ],
        'layout' => "{pager}\n{items}\n{pager}",
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_log',
            //'fecha',
            [
                'attribute' => 'fecha',
                'value' => 'fecha',
                'format' => 'raw',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'fecha',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ])
            ],
            [
                'attribute' => 'hora',
                'value' => 'hora',
                'format' => 'raw',
                'filter' => TimePicker::widget([
                    'model' => $searchModel,
                    'value' => '00:00',
                    'attribute' => 'hora',
                    'pluginOptions' => [
                        'showSeconds' => true,
                        'showMeridian' => true,
                        'minuteStep' => 1,
                        'secondStep' => 5,
                        'defaultTime' => false,
                    ]
                ])
            ],
            //'hora',
            'usuario_id',
            'usuario.nombre']
            ]

    ); ?>

    <?php Pjax::end(); ?>

</div>
