<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\Empresa */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="empresa-form">

    <?php $form = ActiveForm::begin(['options' => ['validateOnSubmit' => true,]]) ?>

    <?= $form->field($model, 'nombre')->textInput(['placeholder' => 'Introduce el nombre de la empresa']) ?>

    <?= $form->field($model, 'direccion')->textInput(['placeholder' => 'Introduce la dirección']) ?>

    <?= $form->field($model, 'cif')->textInput(['placeholder' => 'Introduce el cif']) ?>

    <?= $form->field($model, 'telefono')->textInput(['placeholder' => 'Introduce el telefono']) ?>

    <?= $form->field($model, 'email')->textInput(['placeholder' => 'Introduce el mail']) ?>

    <?= $form->field($departamento, 'id_departamento')->widget(Select2::classname(), [
        'data' => $listaDepartamentos,
        'language' => 'es',
        'options' => [
            'placeholder' => 'Añadir Departamento',
            'id' => 'nombre',
            'selected' => 'selected'],
        'pluginOptions' => [
            'tags' => true,
            'allowClear' => true,
            'maximumInputLength' => 10,
            'multiple' => true
        ],
    ])->label('Lista de Departamentos');

    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

