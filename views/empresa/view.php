<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\empresa */

$this->title = $model->id_empresa;
$this->params['breadcrumbs'][] = ['label' => 'Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="empresa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id_empresa], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id_empresa], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_empresa',
            'nombre',
            'direccion',
            'cif',
            'telefono',
            'email',
            [
                'attribute' => 'departamento_id',
                'value' => 'departamento_id',
                'format' => 'raw',
                'filter' => Select2::widget([
                    'name' => 'state_10',
                    'data' => $listaDepartamentos,
                    'options' => [
                        'multiple' => true
                    ],
                ])
            ],
            'activo',
        ],
    ]) ?>

</div>
