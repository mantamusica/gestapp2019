<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap\BootstrapAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?=
    $this->registerCssFile("@web/css/bootstrap.css", [
        'depends' => [BootstrapAsset::className()]
    ]);
    ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/back/index']],
            [
                'label' => 'Información Usuarios',
                'options'=>['class'=>'dropdown'],
                'items' => [
                    ['label' => 'Empresa', 'url' => ['/empresa/index']],
                    ['label' => 'Pertenecen', 'url' => ['/pertenecen/index']],
                    ['label' => 'Departamento', 'url' => ['/departamento/index']],
                    ['label' => 'Usuario', 'url' => ['/usuario/index']],
                    ['label' => 'Rol', 'url' => ['/rol/index']],
                ]
            ],
            [
                'label' => 'Detalle Tarea',
                'options'=>['class'=>'dropdown'],
                'items' => [
                    ['label' => 'Tarea', 'url' => ['/tarea/index']],
                    ['label' => 'Proyecto', 'url' => ['/proyecto/index']],
                    ['label' => 'Nota', 'url' => ['/nota/index']],
                ]
            ],
            [
                'label' => 'Características Tarea',
                'options'=>['class'=>'dropdown'],
                'items' => [
                    ['label' => 'Categoria', 'url' => ['/categoria/index']],
                    ['label' => 'Estado', 'url' => ['/estado/index']],
                    ['label' => 'Prioridad', 'url' => ['/prioridad/index']],
                ]
            ],
            [
                'label' => 'Listados de Control',
                'options'=>['class'=>'dropdown'],
                'items' => [
                    ['label' => 'Sesiones', 'url' => ['/sesion/index']],
                    ['label' => 'Logs', 'url' => ['/log/index']],
                    ['label' => 'Errores', 'url' => ['/error/index']],
                    ['label' => 'Archivos', 'url' => ['/archivo/index']],
                    ['label' => 'Formularios', 'url' => ['/formulario/index']],
                    ['label' => 'Estadísticas', 'url' => ['/back/index']],
                ]
            ],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/back/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/back/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
