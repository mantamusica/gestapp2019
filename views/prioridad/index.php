<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PrioridadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Prioridades';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="prioridad-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Nueva Prioridad', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'pager' => [
            'firstPageLabel' => 'Primera',
            'lastPageLabel'  => 'Última'
        ],
        'layout' => "{pager}\n{items}\n{pager}",
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_prioridad',
            'nombre',
            'descripcion',
            'activo',

            ['class' => 'yii\grid\ActionColumn',
                'header'=>'Acciones'
            ]
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
