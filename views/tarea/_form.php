<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Prioridad;
use app\models\Proyecto;
use app\models\Categoria;
use app\models\Estado;
use dosamigos\datepicker\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Tarea */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tarea-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?>

    <?php if($dato == 'update') { ?>
        <?=$form->field($model, 'fecha_creacion')->textInput(['maxlength' => true, 'disabled' => true]);
    }?>

    <?= $form->field($model, 'fecha_inicio')->widget(DatePicker::classname(),[
        'options' => ['placeholder' => 'Introduce la fecha inicial ...'],
        'language' => 'es',
        'inline' => false,
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]]) ?>

    <?= $form->field($model, 'fecha_final')->widget(DatePicker::classname(),[
        'options' => ['placeholder' => 'Introduce la fecha final ...'],
        'language' => 'es',
        'inline' => false,
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]]) ?>

    <?php if($dato == 'update') { ?>
        <?=$form->field($model, 'fecha_update')->textInput(['maxlength' => true, 'disabled' => true]);
    }?>

    <?= $form->field($model, 'categoria_id')->dropDownList(
        Categoria::getCategorias(),
        ['prompt'=>'Selecciona una Categoría ...']
    ); ?>

    <?= $form->field($model, 'estado_id')->dropDownList(
        Estado::getEstados(),
        ['prompt'=>'Selecciona un Estado ...']
    ); ?>

    <?= $form->field($model, 'proyecto_id')->dropDownList(
        Proyecto::getProyectos(),
        ['prompt'=>'Selecciona una Proyecto ...']
    ); ?>

    <?= $form->field($model, 'prioridad_id')->dropDownList(
        Prioridad::getPrioridades(),
        ['prompt'=>'Selecciona una Prioridad ...']
    ); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
